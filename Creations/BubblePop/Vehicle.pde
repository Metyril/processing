class Vehicle {
  float x;
  float y;
  float r;
  color c;
  
  boolean growing = true;
  
  float maxSpeed;
  float maxForce;
  PVector vel;
  PVector target;
  PVector acc;
  PVector pos;
  
  Vehicle(float x_, float y_, color c_) {
    x = x_;
    y = y_;
    c = c_;
    r = 10.0;
    maxSpeed = 10.0;
    maxForce = 1.0;
    vel = PVector.random2D();
    target = new PVector(x, y);
    acc = new PVector();
    pos = new PVector(random(width), random(height));
  }
  
  void grow() {
    if (growing) {
      r = r + 0.5;
    }
  }

  boolean edges() {
    return (x + r > width || x -  r < 0 || y + r > height || y -r < 0);
  }
  
  void behaviors() {
    PVector arrive = arrive(target);
    PVector mouse = new PVector(random(width), random(height));
    PVector flee = flee(mouse);
  
    arrive.mult(1);
    flee.mult(fft.calcAvg(20.0, 20000.0) * 5);
  
    applyForce(arrive);
    applyForce(flee);
  }
  
  void applyForce(PVector f) {
    acc.add(f);
  }
  
  void update() {
    pos.add(vel);
    vel.add(acc);
    acc.mult(0);
  }
  
  void display() {
    stroke(c);
    strokeWeight(this.r);
    point(this.pos.x, this.pos.y);
  }
  
  PVector arrive(PVector target) {
    PVector desired = PVector.sub(target, pos);
    float d = desired.mag();
    float speed = maxSpeed;
    if (d < 100) {
      speed = map(d, 0, 100, 0, maxSpeed);
    }
    desired.setMag(speed);
    PVector steer = PVector.sub(desired, vel);
    steer.limit(maxForce);
    return steer;
  }
  
  PVector flee(PVector target) {
    PVector desired = PVector.sub(target, pos);
    float d = desired.mag();
    if (d < 100) {
      desired.setMag(maxSpeed);
      desired.mult(-1);
      PVector steer = PVector.sub(desired, this.vel);
      steer.limit(maxForce);
      return steer;
    } else {
      return new PVector(0.0, 0.0);
    }
  }
}
