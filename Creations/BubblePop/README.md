# BubblePop

Un programme de visualisation musicale pour Processing. Il possède les fonctions suivantes :

- FFT sur le signal d'entrée

- Transformation d'une image bicouleur en une image monochrome avec par-dessus des particules qui reforment le pattern original

- Affichage du résultat dans une scène 2D

- Effets : Les particules "explosent" au rythme de la musique pour revenir à leur place initiale. La projection dépend de l'amplitude moyenne de la musique

  Ce projet se base sur cette vidéo : https://www.youtube.com/watch?v=QHEQuoIKgNE