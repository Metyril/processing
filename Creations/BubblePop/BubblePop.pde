import ddf.minim.*;
import ddf.minim.analysis.*;

Minim minim;
AudioPlayer player;
FFT fft;

ArrayList<Vehicle> vehicles;
PImage img;

void setup() {
  fullScreen();
  surface.setResizable(true);
  frameRate(100);
  img = loadImage("test.jpg");
  
  vehicles = new ArrayList<Vehicle>();
  
  boolean finished = false;
  while (!finished) {
    int total = 10;
    int count = 0;
    int attempts = 0;


    while (count <  total) {
      Vehicle newV = newVehicle();
      if (newV != null) {
        vehicles.add(newV);
        count++;
      }
      attempts++;
      if (attempts > 1000) {
        finished = true;
        break;
      }
    }


    for (Vehicle v : vehicles) {
      if (v.growing) {
        if (v.edges()) {
          v.growing = false;
        } else {
          for (Vehicle other : vehicles) {
            if (v != other) {
              float d = dist(v.x, v.y, other.x, other.y);
              if (d - 2 < v.r + other.r) {
                v.growing = false;
                break;
              }
            }
          }
        }
      }
      v.grow();
    }
  }
  
  minim = new Minim(this);
  player = minim.loadFile("test14.mp3");
  player.loop();
  fft = new FFT(player.bufferSize(), player.sampleRate());
}

void draw() {
  background(255);
  translate(width / 2 - img.width / 2, height / 2 - img.height / 2);

  for (int i = 0; i < vehicles.size(); i++) {
    Vehicle v = vehicles.get(i);
    v.behaviors();
    v.update();
    v.display();
    fft.forward(player.mix);
  }
}

Vehicle newVehicle() {

  float x = random(width);
  float y = random(height);
  
  while(x > img.width || y > img.height) {
    x = random(width);
    y = random(height);
  }
  
  color col = color(255);

  boolean valid = true;
  for (Vehicle v : vehicles) {
    float d = dist(x, y, v.x, v.y);
    int index = int(x) + int(y) * img.width;
    col = img.pixels[index];
    if (d < v.r || col == color(255)) {
      valid = false;
      break;
    }
  }

  if (valid) {
    return new Vehicle(x, y, col);
  } else {
    return null;
  }
}
