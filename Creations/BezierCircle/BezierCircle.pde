int radius = 500;
int pickedFrame = round(random(3000,4000));

void setup() {
  fullScreen();
  frameRate(2000);
  
  background(0);
  smooth();
  translate(width/2, height/2);
  
  fill(255);
  ellipse(0, 0, 2 * radius, 2 * radius);
}

void draw() {
  fill(0);
  rect(0, 0, 450, 50);
  
  if(frameCount == pickedFrame) {
    saveFrame();
    exit();
  }
  
  fill(255);
  textSize(32);
  text(frameCount, 10, 30);
  
  translate(width/2, height/2);
  noFill();  
  
  beginShape();
  
  float q = random(1) * (TWO_PI);
  vertex(radius * cos(q), radius * sin(q));
  q = random(1) * (TWO_PI);
  quadraticVertex(0, 0, radius * cos(q), radius * sin(q));
  
  endShape();
}
