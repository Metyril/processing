import peasy.*;

PeasyCam cam;

import ddf.minim.analysis.*;
import ddf.minim.*;
 
Minim minim;
AudioPlayer player;
FFT fft;

PVector[][] globe;
int total = 100;

float offset = 0;

float m = 0;
float mchange = 0;


void setup() {
  //size(600, 600, P3D);
  fullScreen(P3D);
  cam = new PeasyCam(this, 500);
  colorMode(HSB);
  globe = new PVector[total+1][total+1];
  
  minim = new Minim(this);
  player = minim.loadFile("test3.mp3");
  player.loop();
  
  fft = new FFT(player.bufferSize(), player.sampleRate());
}

float a = 1;
float b = 1;

float supershape(float theta, float m, float n1, float n2, float n3) {
  float t1 = abs((1/a)*cos(m * theta / 4));
  t1 = pow(t1, n2);
  float t2 = abs((1/b)*sin(m * theta/4));
  t2 = pow(t2, n3);
  float t3 = t1 + t2;
  float r = pow(t3, - 1 / n1);
  return r;
}

void draw() {
  
  fft.forward(player.mix);
  
  m = map(sin(mchange), -1, 1, -5, 5);
  mchange += 0.01;
  
  background(0);
  noStroke();
  lights();
  //float r = 200;
  //for (int i = 0; i < total+1; i++) {
  //  float lat = map(i, 0, total, -PI, PI);
  //  float r2 = supershape(lat, m, 0.2, 1.7, 1.7);
  //  //float r2 = supershape(lat, 2, 10, 10, 10);
  //  for (int j = 0; j < total+1; j++) {
  //    float lon = map(j, 0, total, -PI, PI);
  //    float r1 = supershape(lon, m, 0.2, 1.7, 1.7);
  //    float x = r * r1 * cos(lon) * r2 * cos(lat);
  //    float y = r * r1 * sin(lon) * r2 * cos(lat);
  //    float z = r * r2 * sin(lat);
  //    globe[i][j] = new PVector(x, y, z);
  //  }
  //}
  
  float r = 150;
  for (int i = 0; i < total+1; i++) {
    float lat = map(i, 0, total, -PI, PI);
    float r2 = supershape(lat, m, 0.2, fft.getBand(200), fft.getBand(200));
    for (int j = 0; j < total+1; j++) {
      float lon = map(j, 0, total, -PI, PI);
      float r1 = supershape(lon, m, 0.2, 1.7, 1.7);
      float x = r * r1 * cos(lon) * r2 * cos(lat);
      float y = r * r1 * sin(lon) * r2 * cos(lat);
      float z = r * r2 * sin(lat);
      globe[i][j] = new PVector(x, y, z);
    }
  }

  //stroke(255);
  fill(255);
  //noFill();
  offset += 5;
  for (int i = 0; i < total; i++) {
    float hue = map(i, 0, total, 0, 255*6);
    fill((hue + offset) % 255 , 255, 255);
    beginShape(TRIANGLE_STRIP);
    for (int j = 0; j < total+1; j++) {
      PVector v1 = globe[i][j];
      vertex(v1.x, v1.y, v1.z);
    }
    endShape();
  }
}
