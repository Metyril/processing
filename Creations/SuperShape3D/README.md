# SuperShape3D

Un programme de visualisation musicale pour Processing. Il possède les fonctions suivantes :

- FFT sur le signal d'entrée

- Affichage du résultat dans une scène 3D, avec une "supershape" (https://en.wikipedia.org/wiki/Superformula)

- Effets : La couleur et la taille de la supershape dépend de l'amplitude moyenne de la musique

  Ce projet se base sur cette vidéo : https://www.youtube.com/watch?v=ksRoh-10lak