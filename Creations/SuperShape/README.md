# SuperShape

Un programme de visualisation musicale pour Processing. Il possède les fonctions suivantes :

- FFT sur le signal d'entrée

- Affichage du résultat dans une scène 2D, avec des "supershapes" (https://en.wikipedia.org/wiki/Superformula)

- Effets : Le nombre et la couleur des supershapes dépendent de l'amplitude moyenne de la musique

  Ce projet se base sur cette vidéo : https://www.youtube.com/watch?v=ksRoh-10lak