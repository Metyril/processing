import ddf.minim.analysis.*;
import ddf.minim.*;
 
Minim minim;
AudioPlayer player;
FFT fft;

float t = 0.0;
float couleur = 0;
float sides = 0.0;
float x = 0.0;
float y = 0.0;
float rad = 0.0;
int nbHigh;
int strokeType  = round(random(1.0, 10.0));
 
void setup()
{
  //size(500, 500);
  fullScreen();
  
  noFill();
  background(0);
  strokeWeight(2);
  frameRate(500);
 
  minim = new Minim(this);
  player = minim.loadFile("test6.mp3");
  player.loop();
  
  fft = new FFT(player.bufferSize(), player.sampleRate());
}

float r(float theta, float a, float b, float m, float n1, float n2, float n3)
{
  return pow(pow(abs(cos(m * theta / 4.0) / a), n2) + pow(abs(sin(m * theta / 4.0) / b), n3), -1.0 / n1);
}
 
void draw()
{
  clear();
  translate(width / 2, height / 2);
  
  if(nbHigh >= 20) {
    int strokeTypeTemp = strokeType;
    while(strokeTypeTemp == strokeType) {
      strokeTypeTemp = round(random(1.0, 10.0));
    }
    strokeType  = strokeTypeTemp;
  }
 
  fft.forward(player.mix);
  
  nbHigh = 0;

  for(int i = 0; i < fft.specSize(); i++)
  {
    if (fft.getBand(i) >= 30) {
      t = fft.getBand(i);
      nbHigh += 1;
      
      switch(strokeType) {
        case 1 : stroke(lerpColor(color(239, 59, 54), color(255, 255, 255), abs(sin(couleur)))); break;
        case 2 : stroke(lerpColor(color(54, 0, 51), color(11, 135, 147), abs(sin(couleur)))); break;
        case 3 : stroke(lerpColor(color(211, 131, 18), color(168, 50, 121), abs(sin(couleur)))); break;
        case 4 : stroke(lerpColor(color(255, 0, 0), color(255, 255, 255), abs(sin(couleur)))); break;
        case 5 : stroke(lerpColor(color(0, 0, 255), color(255, 255, 255), abs(sin(couleur)))); break;
        case 6 : stroke(lerpColor(color(48, 232, 191), color(255, 130, 53), abs(sin(couleur)))); break;
        case 7 : stroke(lerpColor(color(32, 0, 44), color(203, 180, 212), abs(sin(couleur)))); break;
        case 8 : stroke(lerpColor(color(195, 55, 100), color(29, 38, 113), abs(sin(couleur)))); break;
        case 9 : stroke(lerpColor(color(33, 150, 243), color(244, 67, 54), abs(sin(couleur)))); break;
        case 10 : stroke(lerpColor(color(168, 0, 119), color(102, 255, 0), abs(sin(couleur)))); break;
        default : stroke(255); break;
      }
 
      beginShape();
      
      for (float theta = 0; theta <= 2 * PI; theta += 0.01) {
          rad = r(
          r(theta, t, t, (abs(sin(sides)) * 20.0 + 10.0), 3.0, cos(t), cos(t)),
          r(theta, t, t, (abs(sin(sides)) * 20.0 + 10.0), 3.0, cos(t), cos(t)), // a
          r(theta, t, t, (abs(sin(sides)) * 20.0 + 10.0), 3.0, cos(t), cos(t)), // b
          r(theta, t, t, (abs(sin(sides)) * 20.0 + 10.0), 3.0, cos(t), cos(t)), // m
          r(theta, t, t, (abs(sin(sides)) * 20.0 + 10.0), 3.0, cos(t), cos(t)), // n1
          r(theta, t, t, (abs(sin(sides)) * 20.0 + 10.0), 3.0, cos(t), cos(t)), // n2
          r(theta, t, t, (abs(sin(sides)) * 20.0 + 10.0), 3.0, cos(t), cos(t))  // n3
          );
          
          //rad = r(theta, t, t, (abs(sin(sides)) * 20.0 + 10.0), 3.0, cos(t), cos(t));
          
          x = rad * cos(theta) * 100;
          y = rad * sin(theta) * 100;
          
          vertex(x, y);
      }
      
      endShape(CLOSE);
      
      couleur += 1;
    }
    sides += 0.00001;
  }
}

void stop()
{
  player.close();
  minim.stop();
  super.stop();
}
