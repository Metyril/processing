# Landscape

Un programme de visualisation musicale pour Processing. Il possède les fonctions suivantes :

- FFT sur le signal d'entrée

- Affichage du résultat dans une scène 3D, avec un terrain généré par bruit de Perlin et 2 signaux disposés pour former du relief

- Interpolation des couleurs suivant la hauteur du terrain

  Ce projet n'est pas encore abouti, mon objectif est de coder un projet similaire à celui-ci : https://vimeo.com/2094557?pg=embed&sec=2094557

