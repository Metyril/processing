import ddf.minim.*;
import ddf.minim.analysis.*;
import peasy.*;

Minim minim;
AudioPlayer player;
FFT fft;
PeasyCam cam;

PFont font;
Float[][] terrain;
int nbRow;
float flying = 0;
color c1 = color(64, 11, 11);
color c2 = color(161, 36, 36);
color c3 = color(238, 123, 6);
color c4 = color(255, 169, 4);
color c5 = color(255, 219, 0);

float brightness = 0;

void setup()
{
  fullScreen(P3D);
  frameRate(100);

  cam = new PeasyCam(this, width/2, height/2, (height/2) / tan(PI/6), 0);

  minim = new Minim(this);
  player = minim.loadFile("test13.mp3");
  player.loop();

  fft = new FFT(player.bufferSize(), player.sampleRate());
  fft.logAverages(22, 5);

  nbRow = fft.avgSize() + fft.avgSize() / 2;

  terrain = new Float[nbRow][fft.avgSize()];
  for (int y = 0; y < nbRow; y++) {
    for (int x = 0; x < fft.avgSize(); x++) {
      terrain[y][x] = fft.getAvg(x) * sin((10 * x) / (PI * fft.avgSize()));
    }
    fft.forward(player.mix);
  }
}

void terrainCycle(Float[][] arr) 
{
  flying -= 0.1;
  int i;
  Float[] temp;
  temp = arr[0];
  for (i = 0; i < nbRow - 1; i++) {
    arr[i] = arr[i + 1];
  }
  arr[i] = temp;
  float xoff = 0;
  for (int x = 0; x < fft.avgSize(); x++) {
    arr[i][x] = max(fft.getAvg(x) * sin((10 * x) / (PI * fft.avgSize())), map(noise(xoff, flying), 0, 1, 0, 100));
    xoff += 0.2;
  }
  fft.forward(player.mix);
}

void draw()
{
  background(0);
  noCursor();

  hint(DISABLE_DEPTH_TEST);
  fill(255);
  textSize(20);
  textAlign(CENTER);
  camera();
  text(frameRate, 100, 100);
  hint(ENABLE_DEPTH_TEST);

  translate(width/2 - 500, height/2 + 150, (height/2) / tan(PI/6));
  noFill();

  if (frameCount != 1) {
    terrainCycle(terrain);
  }

  for (int y = 0; y < nbRow - 1; y++) {
    beginShape(TRIANGLE_STRIP);
    for (int x = 0; x < fft.avgSize(); x++) {
      brightness = map(lerp(terrain[y][fft.avgSize() - 1 - x], terrain[y+1][fft.avgSize() - 1 - x], 0.5), 0, 300, 0, 1);

      if (lerp(terrain[y][fft.avgSize() - 1 - x], terrain[y+1][fft.avgSize() - 1 - x], 0.5) < 50) {
        stroke(lerpColor(c1, c2, brightness));
      } else if (lerp(terrain[y][fft.avgSize() - 1 - x], terrain[y+1][fft.avgSize() - 1 - x], 0.5) < 100) {
        stroke(lerpColor(c2, c3, brightness));
      } else if (lerp(terrain[y][fft.avgSize() - 1 - x], terrain[y+1][fft.avgSize() - 1 - x], 0.5) < 250) {
        stroke(lerpColor(c3, c4, brightness));
      } else if (lerp(terrain[y][fft.avgSize() - 1 - x], terrain[y+1][fft.avgSize() - 1 - x], 0.5) < 400) {
        stroke(lerpColor(c4, c5, brightness));
      }

      vertex(500 * x / (fft.avgSize() - 1), -terrain[y][fft.avgSize() - 1 - x], -y * 30);
      vertex(500 * x / (fft.avgSize() - 1), -terrain[y+1][fft.avgSize() - 1 - x], -(y+1) * 30);
    }
    endShape();
    translate(500, 0, 0);
    beginShape(TRIANGLE_STRIP);
    for (int x = 0; x < fft.avgSize(); x++) {
      brightness = map(lerp(terrain[y][x], terrain[y+1][x], 0.5), 0, 300, 0, 1);

      if (lerp(terrain[y][x], terrain[y+1][x], 0.5) < 50) {
        stroke(lerpColor(c1, c2, brightness));
      } else if (lerp(terrain[y][x], terrain[y+1][x], 0.5) < 100) {
        stroke(lerpColor(c2, c3, brightness));
      } else if (lerp(terrain[y][x], terrain[y+1][x], 0.5) < 250) {
        stroke(lerpColor(c3, c4, brightness));
      } else if (lerp(terrain[y][x], terrain[y+1][x], 0.5) < 400) {
        stroke(lerpColor(c4, c5, brightness));
      }

      vertex(500 * x / (fft.avgSize() - 1), -terrain[y][x], -y * 30);
      vertex(500 * x / (fft.avgSize() - 1), -terrain[y+1][x], -(y+1) * 30);
    }
    endShape();
    translate(-500, 0, 0);
  }
}
