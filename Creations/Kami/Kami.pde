import ddf.minim.*;
import ddf.minim.analysis.*;

Minim minim;
AudioPlayer player;
FFT fft;
AudioVisualizer visualizer;

PFont myFont;

float shiftX1;
float shiftY1;
float shiftX2;
float shiftY2;

float angle;
float shift;
int treshold;

String musicTitle;
String musicLength;

void setup()
{
  fullScreen(P2D);
  frameRate(100);
  
  myFont = createFont("SourceHanSerif-Regular", 20);
  textFont(myFont);
  
  musicTitle = "Reol - たい";
  
  minim = new Minim(this);
  player = minim.loadFile(musicTitle + ".mp3");
  player.loop();
  
  musicLength = player.length() / 60000 + ":" + player.length() / 1000 % 60;

  fft = new FFT(player.bufferSize(), player.sampleRate());
  fft.logAverages(22, 5);

  visualizer = new AudioVisualizer(fft, player.mix);
  
  shiftX1 = 0;
  shiftY1 = 0;
  shiftX2 = 0;
  shiftY2 = 0;
  
  angle = 0;
  shift = random(0, 1) > 0.5 ? random(-2, -0.5) : random(0.5, 2);
  treshold = (int) random(0, 360);
}

void draw()
{
  blendMode(ADD);
  background(0);
  noCursor();
  
  fill(150);
  textAlign(LEFT);
  text("神の目", 25, 25);
  text(player.position() / 60000 + ":" + player.position() / 1000 % 60 + " / " + musicLength, 25, height - 25);
  textAlign(RIGHT);
  text("Metyril", width - 25, 25);
  text(musicTitle, width - 25, height - 25);
  
  angle += shift / 5.0;
  
  if(abs(abs(angle) % 360 - treshold) < abs(shift)) {
    shift = random(0, 1) > 0.5 ? random(-2, -0.5) : random(0.5, 2);
    treshold = (int) random(0, 360);
  }
  
  translate(width/2, height/2);
  rotate(radians(angle));
  
  shiftX1 = lerp(shiftX1, map(noise(frameCount / 5), 0, 1, -50, 50) * map(fft.calcAvg(0, 200000), 0, 10, 1, 4), 0.05);
  shiftY1 = lerp(shiftY1, map(noise(frameCount / 5), 0, 1, -50, 50) * map(fft.calcAvg(0, 200000), 0, 10, 1, 4), 0.05);
  shiftX2 = lerp(shiftX2, map(noise(frameCount / 5), 0, 1, -50, 50) * map(fft.calcAvg(0, 200000), 0, 10, 1, 4), 0.05);
  shiftY2 = lerp(shiftY2, map(noise(frameCount / 5), 0, 1, -50, 50) * map(fft.calcAvg(0, 200000), 0, 10, 1, 4), 0.05);
      
  visualizer.getShape().setFill(color(0, 0, 255));
  shape(visualizer.getShape(), 0, 0);
  
  visualizer.getShape().setFill(color(0, 255, 0));
  shape(visualizer.getShape(), shiftX1, shiftY1);
  
  visualizer.getShape().setFill(color(255, 0, 0));
  shape(visualizer.getShape(), -shiftX2, shiftY2);
  
  visualizer.update();
  
  if (keyPressed) {
    if (key == 's') {
      saveFrame("frame-######.jpg");
    }
  }
}
