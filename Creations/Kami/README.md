# Kami

Un programme de visualisation musicale pour Processing. Il possède les fonctions suivantes :

- FFT sur le signal d'entrée
- Affichage du résultat dans une scène 2D, avec 4 signaux disposés sur 3 cercles RGB
- Effets : Les disques se déforment suivant l'amplitude la musique. Ils se déplacent légèrement et aléatoirement; le décalage créé retire la superposition des couleurs et donne un aspect "glitch" au rendu
- Les informations sur la musique jouée sont affichés dans les coins de l'écran
- Il est possible de prendre une capture de fenêtre à tout moment

[Vidéo de démonstration](https://youtu.be/tFa2KVY2PMY)