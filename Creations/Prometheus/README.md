# Prometheus

Un programme de visualisation musicale pour Processing. Il possède les fonctions suivantes :

- FFT sur le signal d'entrée

- Affichage du résultat dans une scène 2D, avec 4 signaux disposés sur 3 cercles RGB dans un "oeil"

- Effets : Les disques se déforment suivant l'amplitude la musique. Ils se déplacent légèrement et aléatoirement; le décalage créé retire la superposition des couleurs et donne un aspect "glitch" au rendu

- Les disques se déplacent sur des courbes de Bézier générées en continu et aléatoirement dans l'oeil

  Le but de ce projet est de se rapprocher de l'aspect visuel de cette vidéo, qui m'a aussi servie d'inspiration : https://www.youtube.com/watch?v=H1IlVFXdFyc