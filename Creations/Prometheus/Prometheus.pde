import ddf.minim.*;
import ddf.minim.analysis.*;

Minim minim;
AudioPlayer player;
FFT fft;
AudioVisualizer visualizer;

PShape eye;
PShape path;

float anchorPoint1X;
float anchorPoint1Y;
float anchorPoint2X;
float anchorPoint2Y;
float ctrlPoint1X;
float ctrlPoint1Y;
float ctrlPoint2X;
float ctrlPoint2Y;

int frames;
int resetPath;
int waitTime;

float t;
float x;
float y;

void setup()
{
  fullScreen(P2D);
  frameRate(100);
  
  minim = new Minim(this);
  player = minim.loadFile("gringe.mp3");
  player.loop();

  fft = new FFT(player.bufferSize(), player.sampleRate());
  fft.logAverages(22, 5);

  visualizer = new AudioVisualizer(fft, player.mix);
  
  frames = 0;
  resetPath = 500;
  waitTime = 100;
  
  anchorPoint1X = width / 2;
  anchorPoint1Y = height / 2;
  anchorPoint2X = width / 2;
  anchorPoint2Y = height / 2;
  
  eye = createShape();
  eye.beginShape();
  eye.stroke(0, 249, 255);
  eye.strokeWeight(15);
  eye.noFill();
  
  eye.vertex(width / 8, height / 2);
  eye.bezierVertex(width / 3, 0,
                   2 * width / 3, 0,
                   7 * width / 8, height / 2);
  eye.bezierVertex(2 * width / 3, height,
                   width / 3, height,
                   width / 8, height / 2);
  eye.endShape();
  
  path = updatePath();
}

void draw()
{
  println(frameRate);
  blendMode(ADD);
  background(0);
  noCursor();
  
  if(frames <= resetPath) {
    t = frames / float(resetPath);
    x = bezierPoint(anchorPoint1X, ctrlPoint1X, ctrlPoint2X, anchorPoint2X, t);
    y = bezierPoint(anchorPoint1Y, ctrlPoint1Y, ctrlPoint2Y, anchorPoint2Y, t);
    
    frames ++;
  } else if(frames == resetPath + waitTime) {
    frames = 0;
    path = updatePath();
  } else if (frames > resetPath) {
    frames ++;
  }
  
  shape(eye, 0, 0);
  
  //visualizer.getShape().rotate(0.2);
  
  visualizer.getShape().setFill(color(0, 0, 255));
  shape(visualizer.getShape(), x, y);
  
  visualizer.getShape().setFill(color(0, 255, 0));
  shape(visualizer.getShape(), x + map(noise(frameCount / 5), 0, 1, -10, 10), y + map(noise(frameCount / 5), 0, 1, -10, 10));
  
  visualizer.getShape().setFill(color(255, 0, 0));
  shape(visualizer.getShape(), x - map(noise(frameCount / 5), 0, 1, -10, 10), y + map(noise(frameCount / 5), 0, 1, -10, 10));
  //shape(path, 0, 0);
  
  visualizer.update();
}

PShape updatePath() {
  waitTime = (int) random(50, 300);
  
  anchorPoint1X = anchorPoint2X;
  anchorPoint1Y = anchorPoint2Y;
  
  ctrlPoint1X = random(width / 2 - height / 6, width / 2 + height / 6);
  ctrlPoint1Y = random(height / 2 - height / 6, height / 2 + height / 6);
  ctrlPoint2X = random(width / 2 - height / 6, width / 2 + height / 6);
  ctrlPoint2Y = random(height / 2 - height / 6, height / 2 + height / 6);
  
  PShape path = createShape();
  
  path.beginShape();
  path.stroke(255);
  path.noFill();
  
  path.vertex(anchorPoint1X, anchorPoint1Y);
  
  anchorPoint2X = random(width / 2 - height / 6, width / 2 + height / 6);
  anchorPoint2Y = random(height / 2 - height / 6, height / 2 + height / 6);
  
  path.bezierVertex(ctrlPoint1X, ctrlPoint1Y,
                    ctrlPoint2X, ctrlPoint2Y,
                    anchorPoint2X, anchorPoint2Y);
  path.endShape();
  
  return path;
}
