class AudioVisualizer {
  private FFT fft;
  private AudioBuffer playerMode;
  private Float[] data;
  private PShape shape;
  private float cosFormula;
  private float sinFormula;
  private int shapeRadius;
  private float shapeShift;
  private float shapeScale;

  AudioVisualizer(FFT _fft, AudioBuffer _playerMode) {
    this.fft = _fft;
    this.playerMode = _playerMode;
    
    this.data = new Float[this.fft.avgSize()];
    
    for (int x = 0; x < this.fft.avgSize(); x++) {
      this.data[x] = -this.fft.getAvg(x);
    }
    
    this.shapeRadius = 200;
    this.shapeShift = -0.02;
    this.shapeScale = 1;
    
    this.shape = createShape();
    this.shape.beginShape();
    this.shape.fill(255);
    
    for(int i = 0; i < 4; i ++) {
      for (int x = 0; x < this.fft.avgSize(); x++) {
        this.cosFormula = cos(i * PI / 2 + (float)x / this.fft.avgSize() * PI / 2);
        this.sinFormula = sin(i * PI / 2 + (float)x / this.fft.avgSize() * PI / 2);
      
        this.shape.curveVertex(this.shapeRadius * this.cosFormula, this.shapeRadius * this.sinFormula);
      }
    }
    
    this.shape.endShape();
    this.fft.forward(this.playerMode);
  }
  
  void update() {
    this.updateShape();
    this.updateData();
  }
  
  void updateShape() {
    //this.shapeShift = map(noise(frameCount), 0, 1, -0.001, 0.001);
    //this.shapeScale = map(this.fft.calcAvg(0, 200000), 1, 10, 1, 3);
    
    for(int i = 0; i < 4; i ++) {
      for (int x = 0; x < this.fft.avgSize(); x++) {
        this.cosFormula = cos(i * PI / 2 + (float)x / this.fft.avgSize() * PI / 2 + this.shapeShift * this.data[x]);
        this.sinFormula = sin(i * PI / 2 + (float)x / this.fft.avgSize() * PI / 2 + this.shapeShift * this.data[x]);
      
        //this.shape.setVertex(x + i * this.fft.avgSize(), this.shapeRadius * this.cosFormula, this.shapeRadius * this.sinFormula);
        this.shape.setVertex(x + i * this.fft.avgSize(),
                             this.shapeRadius * this.shapeScale * this.cosFormula + ((i % 3 == 0)? 5 : -5) * this.data[x],
                             this.shapeRadius * this.shapeScale * this.sinFormula + ((i % 4 < 2)? 5 : -5) * this.data[x]);
      }
    }
  }

  void updateData() {
    for (int x = 0; x < this.fft.avgSize(); x++) {
      if (x < 20) {
        this.data[x] = 0.8 * this.data[x] - 0.2 * this.fft.getAvg(x) / 3;
      } else {
        this.data[x] = 0.8 * this.data[x] - 0.2 * this.fft.getAvg(x);
      }
    }

    this.fft.forward(this.playerMode);
  }
  
  PShape getShape() {
    return this.shape;
  }
}
