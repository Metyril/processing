# HyperSpace

Un programme de visualisation musicale pour Processing. Il possède les fonctions suivantes :

- FFT sur le signal d'entrée
- Affichage du résultat dans une scène 3D, avec 4 signaux disposés pour former un tunnel
- Effets : Des particules traversent de part et d'autre l'écran pour un look "Hyper vitesse"; leur vitesse et leur couleur dépendent de l'amplitude moyenne

[Vidéo de démonstration](https://youtu.be/D_MPrcC4eGM)