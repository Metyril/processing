import ddf.minim.*;
import ddf.minim.analysis.*;
import peasy.*;

Minim minim;
AudioPlayer player;
FFT fftLeft;
FFT fftRight;
PeasyCam cam;

Float[][] terrainLeft;
Float[][] terrainRight;
Star[] stars = new Star[600];
int nbRow;
float speed;

void setup()
{
  fullScreen(P3D);
  frameRate(100);
  cam = new PeasyCam(this, 250, -150, 150, 50);

  for (int i = 0; i < stars.length; i++) {
    stars[i] = new Star();
  }

  minim = new Minim(this);
  player = minim.loadFile("test13.mp3");
  player.loop();

  fftLeft = new FFT(player.bufferSize(), player.sampleRate());
  fftLeft.logAverages(22, 5);

  fftRight = new FFT(player.bufferSize(), player.sampleRate());
  fftRight.logAverages(22, 5);

  nbRow = (fftLeft.avgSize() + fftRight.avgSize()) / 2;

  terrainLeft = new Float[nbRow][fftLeft.avgSize()];
  for (int y = 0; y < nbRow; y++) {
    for (int x = 0; x < fftLeft.avgSize(); x++) {
      terrainLeft[y][x] = fftLeft.getAvg(x) * sin((10 * x) / (PI * fftLeft.avgSize()));
    }
    fftLeft.forward(player.left);
  }

  terrainRight = new Float[nbRow][fftRight.avgSize()];
  for (int y = 0; y < nbRow; y++) {
    for (int x = 0; x < fftRight.avgSize(); x++) {
      terrainRight[y][x] = fftRight.getAvg(x) * sin((10 * x) / (PI * fftRight.avgSize()));
    }
    fftRight.forward(player.right);
  }
}

void terrainCycle(Float[][] arr, boolean left) 
{
  int i;
  Float[] temp;
  temp = arr[0];
  for (i = 0; i < nbRow - 1; i++) {
    arr[i] = arr[i + 1];
  }
  arr[i] = temp;
  if (left) {
    for (int x = 0; x < fftLeft.avgSize(); x++) {
      arr[i][x] = fftLeft.getAvg(x) * sin((10 * x) / (PI * fftLeft.avgSize()));
    }
    fftLeft.forward(player.left);
  } else {
    for (int x = 0; x < fftRight.avgSize(); x++) {
      arr[i][x] = fftRight.getAvg(x) * sin((10 * x) / (PI * fftLeft.avgSize()));
    }
    fftRight.forward(player.right);
  }
}

void draw()
{
  if (keyPressed && player.isPlaying() && key == 'a' || key == 'A') {
    player.pause();
  } else if (keyPressed && !player.isPlaying() && key == 'z' || key == 'Z') {
    player.loop();
  }
  if (player.isPlaying()) {
    background(0);
    rotateX(PI/2);
    noFill();
    noCursor();

    if (frameCount != 1) {
      terrainCycle(terrainLeft, true);
      terrainCycle(terrainRight, false);
    }

    for (int y = 0; y < nbRow; y++) {
      beginShape();
      for (int x = 0; x < fftLeft.avgSize(); x++) {
        stroke(terrainLeft[y][x] * 5);
        vertex(500 * x / (fftLeft.avgSize() - 1), -y * 30, terrainLeft[y][x]);
      }
      endShape();
      beginShape();
      for (int x = 0; x < fftLeft.avgSize(); x++) {
        stroke(terrainLeft[y][fftLeft.avgSize() - 1 - x] * 5);
        vertex(500 * x / (fftLeft.avgSize() - 1), -y * 30, terrainLeft[y][fftLeft.avgSize() - 1 - x]);
      }
      endShape();
      translate(0, 0, 300);
      beginShape();
      for (int x = 0; x < fftRight.avgSize(); x++) {
        stroke(terrainRight[y][x] * 5);
        vertex(500 * x / (fftRight.avgSize() - 1), -y * 30, -terrainRight[y][x]);
      }
      endShape();
      beginShape();
      for (int x = 0; x < fftRight.avgSize(); x++) {
        stroke(terrainRight[y][fftRight.avgSize() - 1 - x] * 5);
        vertex(500 * x / (fftRight.avgSize() - 1), -y * 30, -terrainRight[y][fftRight.avgSize() - 1 - x]);
      }
      endShape();
      translate(0, 0, -300);
    }

    speed = map((fftLeft.calcAvg(20.0, 20000.0) + fftRight.calcAvg(20.0, 20000.0)) * 50.0, 0, width, 0, 50);
    rotateX(PI/2);
    for (int i = 0; i < stars.length; i++) {
      stars[i].update();
      stars[i].show();
    }
  }
}
