import ddf.minim.*;
import ddf.minim.analysis.*;
import peasy.*;

Minim minim;
AudioPlayer player;
FFT fft;
PeasyCam cam;

PFont font;
Float[][] terrain;
int nbRow;
float speed = 0.005;
float light = 255.0;

void setup()
{
  fullScreen(P3D);
  frameRate(100);
  
  cam = new PeasyCam(this, width/2, height/2, (height/2) / tan(PI/6), 0);
  
  font = loadFont("KDWilliam-Regular-40.vlw");
  textFont(font);

  minim = new Minim(this);
  //player = minim.loadFile("KUURO - Swarm VIP - 01 Swarm VIP.mp3");
  player = minim.loadFile("test13.mp3");
  player.loop();

  fft = new FFT(player.bufferSize(), player.sampleRate());
  fft.logAverages(22, 5);

  nbRow = fft.avgSize();

  terrain = new Float[nbRow][fft.avgSize()];
  for (int y = 0; y < nbRow; y++) {
    for (int x = 0; x < fft.avgSize(); x++) {
      terrain[y][x] = fft.getAvg(x) * sin((10 * x) / (PI * fft.avgSize()));
    }
    fft.forward(player.mix);
  }
}

void terrainCycle(Float[][] arr) 
{
  int i;
  Float[] temp;
  temp = arr[0];
  for (i = 0; i < nbRow - 1; i++) {
    arr[i] = arr[i + 1];
  }
  arr[i] = temp;
  for (int x = 0; x < fft.avgSize(); x++) {
    arr[i][x] = fft.getAvg(x) * sin((10 * x) / (PI * fft.avgSize()));
  }
  fft.forward(player.mix);
}

void draw()
{
  background(0);
  noCursor();
  
  light = max(lerp(light, fft.calcAvg(20.0, 20000.0) * 40, 0.1), 50);
  
  hint(DISABLE_DEPTH_TEST);
  fill(light);
  textSize(20);
  textAlign(CENTER);
  camera();
  text("Metyril", width/2, height/2);
  hint(ENABLE_DEPTH_TEST);
  
  speed = lerp(speed, 0.005 * fft.calcAvg(20.0, 20000.0), 0.1);  
  cam.rotateZ(speed);
  
  translate(width/2 - 250, height/2 - 250, (height/2) / tan(PI/6));
  noFill();

  if (frameCount != 1) {
    terrainCycle(terrain);
  }

  for (int y = 0; y < nbRow; y++) {
    beginShape();
    for (int x = 0; x < fft.avgSize(); x++) {
      stroke(terrain[y][x] * 5);
      vertex(terrain[y][x], 500 * x / (fft.avgSize() - 1), -y * 30);
    }
    endShape();
    translate(500, 0, 0);
    beginShape();
    for (int x = 0; x < fft.avgSize(); x++) {
      stroke(terrain[y][fft.avgSize() - 1 - x] * 5);
      vertex(-terrain[y][fft.avgSize() - 1 - x], 500 * x / (fft.avgSize() - 1), -y * 30);
    }
    endShape();
    translate(-500, 0, 0);
    beginShape();
    for (int x = 0; x < fft.avgSize(); x++) {
      stroke(terrain[y][fft.avgSize() - 1 - x] * 5);
      vertex(500 * x / (fft.avgSize() - 1), terrain[y][fft.avgSize() - 1 - x], -y * 30);
    }
    endShape();
    translate(0, 500, 0);
    beginShape();
    for (int x = 0; x < fft.avgSize(); x++) {
      stroke(terrain[y][x] * 5);
      vertex(500 * x / (fft.avgSize() - 1), -terrain[y][x], -y * 30);
    }
    endShape();
    translate(0, -500, 0);
  }
}
