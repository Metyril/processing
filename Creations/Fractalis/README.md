# Fractalis

Un programme de visualisation musicale pour Processing. Il possède les fonctions suivantes :

- FFT sur le signal d'entrée
- Affichage du résultat dans une scène 3D, avec 4 signaux disposés pour former un tube
- Effets : Les transformations et la couleur du texte central dépendent de l'amplitude moyenne de la musique
- Lissage par interpolation des variations de couleur et d'amplitude

[Vidéo de démonstration](https://youtu.be/bV7FUrtiEbU)