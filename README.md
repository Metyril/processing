# Processing

Ce répertoire contient l'ensemble de mes programmes écrit en Processing (et peut-être bientôt en p5.js). Il s'agit quasi-exclusivement de projets pour la visualisation musicale, mais je ne me limite pas à ce domaine-là. Chaque dossier contient une brève description de ce que l'on peut y retrouver.