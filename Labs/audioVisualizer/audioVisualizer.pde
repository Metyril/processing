import ddf.minim.*;
import ddf.minim.analysis.*;

Minim minim;
AudioPlayer player;
FFT fft;
AudioData data;

void setup()
{
  fullScreen();
  frameRate(100);

  minim = new Minim(this);
  player = minim.loadFile("test17.mp3");
  player.loop();

  fft = new FFT(player.bufferSize(), player.sampleRate());
  fft.logAverages(22, 5);

  data = new AudioData(fft, player.mix);
}

void draw()
{
  background(0);
  noCursor();

  data.update();
}
