class AudioData {
  FFT fft;
  AudioBuffer playerMode;
  Float[] data;

  AudioData(FFT _fft, AudioBuffer _playerMode) {
    fft = _fft;
    playerMode = _playerMode;
    
    this.data = new Float[this.fft.avgSize()];
    
    for (int x = 0; x < this.fft.avgSize(); x++) {
      this.data[x] = -this.fft.getAvg(x);
    }

    this.fft.forward(this.playerMode);
  }

  void update() {
    for (int x = 0; x < this.fft.avgSize(); x++) {
      if (x < 20) {
        this.data[x] = 0.8 * this.data[x] - 0.2 * this.fft.getAvg(x) / 3;
      } else {
        this.data[x] = 0.8 * this.data[x] - 0.2 * this.fft.getAvg(x);
      }
    }

    this.fft.forward(this.playerMode);
  }
}
