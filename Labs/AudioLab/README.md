# AudioLab

Projet temporaire d'idées pour la visualisation audio en Processing. Actuellement, on y retrouve les fonctions suivantes :

- FFT sur le signal d'entrée (Stéréo si disponible)
- Affichage 2D du résultat
- Effet miroir et lissage des variations