import ddf.minim.*;
import ddf.minim.analysis.*;
import peasy.*;

Minim minim;
AudioPlayer player;
FFT fftLeft;
FFT fftRight;

Float[] terrainLeft;
Float[] terrainRight;

void setup()
{
  fullScreen();
  frameRate(100);

  minim = new Minim(this);
  player = minim.loadFile("test1.mp3");
  player.loop();

  fftLeft = new FFT(player.bufferSize(), player.sampleRate());
  fftLeft.logAverages(22, 5);

  fftRight = new FFT(player.bufferSize(), player.sampleRate());
  fftRight.logAverages(22, 5);

  terrainLeft = new Float[fftLeft.avgSize()];
  for (int x = 0; x < fftLeft.avgSize(); x++) {
    terrainLeft[x] = -fftLeft.getAvg(x);
  }

  terrainRight = new Float[fftRight.avgSize()];
  for (int x = 0; x < fftRight.avgSize(); x++) {
    terrainRight[x] = -fftRight.getAvg(x);
  }

  fftLeft.forward(player.left);
  fftRight.forward(player.right);
}

void draw()
{
  background(0);
  stroke(255);
  noCursor();
  noFill();

  translate(-35, height - 200);

  beginShape();
  for (int x = 0; x < fftLeft.avgSize(); x++) {
    curveVertex((width/2) * x / fftLeft.avgSize(), 5 * terrainLeft[x]);
  }
  curveVertex(width/2, 0);
  for (int x = fftRight.avgSize() - 1; x >= 0; x--) {
    curveVertex(width - width/2 * x / fftRight.avgSize(), 5 * terrainRight[x]);
  }
  endShape();

  for (int x = 0; x < fftLeft.avgSize(); x++) {
    if (x < 20) {
      terrainLeft[x] = 0.8 * terrainLeft[x] - 0.2 * fftLeft.getAvg(x) / 3;
    } else {
      terrainLeft[x] = 0.8 * terrainLeft[x] - 0.2 * fftLeft.getAvg(x);
    }
  }
  for (int x = 0; x < fftRight.avgSize(); x++) {
    if (x < 20) {
      terrainRight[x] = 0.8 * terrainRight[x] - 0.2 * fftRight.getAvg(x) / 3;
    } else {
      terrainRight[x] = 0.8 * terrainRight[x] - 0.2 * fftRight.getAvg(x);
    }
  }

  fftLeft.forward(player.left);
  fftRight.forward(player.right);
}
