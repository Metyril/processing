PGraphics canvas;
PShape disc;

void setup() {
  size(500, 500, P2D);
  frameRate(100);
  
  disc = createShape(ELLIPSE, 100, 100, 100, 100);
  disc.setFill(255);
  
  canvas = createGraphics(300, 300);
  canvas.beginDraw();
  canvas.shape(disc);
  canvas.endDraw();
}

void draw() {
  background(0);
  
  image(canvas, width / 2 - 100, height / 2 - 100);
  
  if(mousePressed) {
    canvas.loadPixels();
    for (int i = 0; i < 300*300; i++) {
      if(canvas.pixels[i] != 0x0) {
        canvas.pixels[i] = color(255, 0, 0);
      }
    }
    canvas.updatePixels();
  } else {
    canvas.loadPixels();
    for (int i = 0; i < 300*300; i++) {
      if(canvas.pixels[i] != 0x0) {
        canvas.pixels[i] = color(255);
      }
    }
    canvas.updatePixels();
  }
}
